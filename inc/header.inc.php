<?php

	/*
	 * 
	 * @Author: Gaspard Rosay
	 * @Web: www.gaspard-rosay.ch
	 * @Date: 2014
	 * @Name: Template
	 * @PageInfo:	Contain all inclusions and arrays etc
	 * @Modif:		Moving configs to settings.inc.php
	 */

$sIncludeJS		= "";
$sIncludeCSS	= "";
$aCSS			= array();
$aJS			= array();

require_once('settings.inc.php');

foreach($aCSS as $key => $val){
	$sIncludeCSS .= "<link rel='stylesheet' href='$val'>";
}
foreach($aJS as $key => $val){
	$sIncludeJS .= "<script src='$val'></script>";
}


# HTML Header for website
# You can add here your Meta balises
echo "	<!DOCTYPE html>
			<html>
				<head>
					<title>$sSiteTitle</title>		
					$sIncludeCSS
					$sIncludeJS
				</head>
				
				<body ".($bFixedNavs ? "style='padding-top:70px; padding-bottom:70px;'" : null).">
				
				";

# Set $bShowHeader to false on the page where you don't want to have the navbar
# It's required when a page is called in AJAX (otherwise you'll have two or more navbar on the page)
if($bShowHeader === false){
	$bNavbar = false;
}

if($bNavbar === true){
	echo "<nav class='navbar navbar-inverse ".($bFixedNavs ? "navbar-fixed-top" : null)."' role='navigation'>
	<div class='container-fluid'>
	  <!-- Brand and toggle get grouped for better mobile display -->
	  <div class='navbar-header'>
		<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-5'>
		  <span class='sr-only'>Toggle navigation</span>
		  <span class='icon-bar'></span>
		  <span class='icon-bar'></span>
		  <span class='icon-bar'></span>
		</button>
		<a class='navbar-brand' href='index.php'>$sSiteTitle</a>
	  </div>
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-5'>
		<ul class='nav navbar-nav'>
		  ";
			foreach($aPages as $key => $val){
				if($val['menu'] == true){
					echo "<li><a href='index.php?p=$key'>".$val['Title']."</a>";
				}
			}
	echo "
		</ul>
		<form class='navbar-form navbar-right' role='search'>
		  
		</form>
	  </div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>";
}

?>