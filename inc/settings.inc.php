<?php

	/*
	 * 
	 * @Author: Gaspard Rosay
	 * @Web: www.gaspard-rosay.ch
	 * @Date: 2014
	 * @Name: Template
	 * @PageInfo:	Contain all settings
	 */

# ToDo: Set here the site title...
$sSiteTitle		= "Your Site Title";

# ToDo: Set this to false if you don't want to have a bootstrap navbar on your website
$bNavbar		= true;

# ToDo: Set this to false if you don't want to have a footer on your website
$bFooter		= true;

# ToDo: Set this to false if you don't want your footer and navbar to be fixed
$bFixedNavs		= true;

# ToDo: Add in $aCSS all your CSS files
$aCSS[]			= "libs/bootstrap/css/bootstrap.css";
$aCSS[]			= "libs/bootflat/css/bootflat.css";
$aCSS[]			= "css/general.css";

# ToDo: Add in $aJS all your JS files
$aJS[]			= "libs/jquery/jquery111.js";
$aJS[]			= "libs/bootstrap/js/bootstrap.js";
$aJS[]			= "libs/bootflat/js/icheck.min.js";
$aJS[]			= "libs/bootflat/js/jquery.collapse.js";
$aJS[]			= "js/footer.js";

#### Pages Array
# ToDo: Add your pages to this array as described below
# This table contain a list of all your page - Format "key" => array("link" => "link_to_your_page.php", "Title" => "Title of your page", "menu" => true (or false))
# The "key" is the word that will be used in the visible link (by example: index.php?p=pageexample)
# The "menu" is set to true if you want this page to appear in the main menu
$aPages = array(

	"index"			=> array("link" => "pages/index.php", "Title" => "Home page", "menu" => false),
	"pageexample"	=> array("link" => "pages/page1.php", "Title" => "Page example", "menu" => true)
	
);
####


?>