/*
 * @Author: Gaspard Rosay
 * Contain all JS scripts to have a full footer only at the bottom of the page
 * 
 */

// Document's ready function
$(document).ready(function(){
	// Creating some variable for elements that we'll use many times
    footerfull     = $('#footer_full');
    footershort     = $('#footer_reducted');
  
    // We'll listen for the scroll event
    $(window).trigger('scroll');
 
    $(window).scroll(function(event){
		
        // Thoses values are defined when the page is loaded
        documentHeight = $(document).height();
		windowHeight = $(window).height();
		currentScroll = $(window).scrollTop();
		
        // Showing or hiding the full header if we're or not at the bottom
        if( documentHeight <= (currentScroll + windowHeight )){
			console.log('ici');
			footershort.hide();
			footerfull.slideDown();
		} 
		else if(documentHeight > (currentScroll + windowHeight)){
			footershort.show();
			footerfull.slideUp();
		}
    });
	
	//
	//////
});