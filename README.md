SimpleSiteTemplate
==================

## See it live

You can now see it running live here: http://www.gaspard-rosay.ch/github/simplesitetemplate/index.php


## What is it ?

This simple template has for purpose to simplify and accelerate the development of website by intergrating frameworks like jQuery, Bootstrap, ...

## How does it works ?

To use this template, you just need to place all the files at the top level of your site.

## Configuration

All the major settings are in one file: _inc/settings.inc.php_.
You juste have to modify some things like the site title, if you want the navbar or not. Also, you can add other CSS or JS files simply by adding them in the php arrays.

The navigation is simple: you just have to create your pages in the subfolder of your choice (ideally in pages) and then, add your pages details in the "pages" PHP array in the header. You must set a _key_, a _title_ and other things for each pages (by example if you want or not this page to figure on the top menu).

## Disclaimer
Of course, Bootstrap and all others libraries integrated in this template are not mine and I don't want to take any advantage of them.

Also, this template isn't perfect and is just a simple files/directories organization to create new websites a little bit faster.

## Credits
__Author__: Gaspard Rosay

__WebSite__: www.gaspard-rosay.ch

## Framworks

__Bootstrap__ _v3.3.1_: http://getbootstrap.com

__jQuery__ _v1.11_: http://jquery.com

__BootFlat__ _v2.2.0_: http://bootflat.github.io 

## ToDo

* Update BootFlat
* Add my personals MySQL/MySQLi libraries