<?php
	/*
	 * 
	 * @Author: Gaspard Rosay
	 * @Web: www.gaspard-rosay.ch
	 * @Date: 2014
	 * @Name: Template
	 * 
	 * 
	 */

echo "<div class='container-fluid'>
	<h1>Your first page</h1>
	<p>Well, it looks like you're on your first page ! This is a pure example. You can delete it or modify it. Enjoy !</p>
</div>";

?>