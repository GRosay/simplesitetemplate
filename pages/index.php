<?php
	/*
	 * 
	 * @Author: Gaspard Rosay
	 * @Web: www.gaspard-rosay.ch
	 * @Date: 2014
	 * @Name: Template
	 * @PageInfo:	This page we'll be your home page.
	 *				Feel free to add HTML or PHP content.
	 * 
	 */

echo "<div class='container-fluid'>
			<h1>This is your home page</h1>
			<p>Congratulation ! Your website is operational :) !</p>
			<p>Now, feel free to add your content...</p>
		</div>";

?>